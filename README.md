# List_Installed_Jen_Plugins

To List and Store all Plugins from Existing Jenkins and export it to another jenkins 

---
## First export your Jenkins URL in ENV
## Then Run this curl cmd to extract list from a JSON Files and store it in a text file
```
export JENKINS_URL=http://192.168.33.10:8080

curl -sSL "$JENKINS_URL/pluginManager/api/xml?depth=1&xpath=/*/*/shortName|/*/*/version&wrapper=plugins" | perl -pe 's/.*?<shortName>([\w-]+).*?<version>([^<]+)()(<\/\w+>)+/\1 \2\n/g'|sed 's/ /:/'  > plugins.txt

```

---
## -- Tips:
 * \w is a special class called "word characters". It is shorthand for [a-zA-Z0-9_], so it will match:
    * a-z (all lowercase letters)
    * A-Z (all uppercase letters)
    * 0-9 (all digits)
    * _ (an underscore)

    * The class you are asking about, [\w-], is a class consisting of \w and -. So it will match the above list, plus hyphens (-).

* Exactly as written, [\w-], this regex would match a single character, as long as it's in the above list, or is a dash.

* If you were to add a quantifier to the end, e.g. [\w-]* or [\w-]+, then it would match any of these strings:
